# rust-exec



## Pour build la dll depuis un windows

```
cargo build --release
```

## Pour build depuis un linux

Avec cargo 
```
rustup target add x86_64-pc-windows-gnu
cargo build --target x86_64-pc-windows-gnu
```

Avec cross (utilise docker)
```
cargo install cross
cross build x86_64-pc-windows-gnu
```
