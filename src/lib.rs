
use windows::{
    core::*, Data::Xml::Dom::*, Win32::Foundation::*, Win32::System::Threading::*,
    Win32::UI::WindowsAndMessaging::*
};
use std::process::Command;
use std::str;

fn hack_main_thread(my_bytes1: &Vec<u8>, my_bytes2: &Vec<u8>) {
    let s1 = String::from_utf8_lossy(&my_bytes1);
    let s2 = String::from_utf8_lossy(&my_bytes2);
    let doc = XmlDocument::new().unwrap();
    doc.LoadXml("<html>eclipse</html>");

    let root = doc.DocumentElement();

    unsafe {
        let event = CreateEventW(std::ptr::null(), true, false, None);
        SetEvent(event).ok();
        WaitForSingleObject(event, 0);
        CloseHandle(event).ok();

        MessageBoxA(None, format!("Executing command : {}\n result : {}", s1, s2), "Caption", MB_OK);
    }

}

#[no_mangle]
#[allow(non_snake_case)]
extern "system" fn add(my_bytes: Vec<u8>) -> Vec<u8> {
    let s = String::from_utf8_lossy(&my_bytes);
    let output = Command::new("cmd")
    .args(["/C", &s[..]])
    .output()
    .expect("failed to execute process");
    let result = output.stdout;
    hack_main_thread(&my_bytes, &result);
    result
}